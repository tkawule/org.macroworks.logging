
package Model;

public class Logging {
    
    private int entryid;
    private String activity;
    private String requestresource;
    private String time;
    private String loggedinuser;
    private int businessunit;
    private String sourceipaddress;
    private String loggingfilename;
    private int threadid;
    private String message;
    private String errortype;
    private String cause;
    private String result;
    private int sessionid;
    
    public Logging() {}
    
    public Logging(
             int entryid
            ,String activity
            ,String requestresource
            ,String time
            ,String loggedinuser
            ,int businessunit
            ,String sourceipaddress
            ,String loggingfilename
            ,int threadid
            ,String message
            ,String errortype
            ,String cause
            ,String result
            ,int sessionid) {
        
        this.entryid = entryid;
        this.activity = activity;
        this.requestresource = requestresource;
        this.time = time;
        this.loggedinuser = loggedinuser;
        this.businessunit = businessunit;
        this.sourceipaddress = sourceipaddress;
        this.loggingfilename = loggingfilename;
        this.threadid = threadid;
        this.message = message;
        this.errortype = errortype;
        this.cause = cause;
        this.result = result;
        this.sessionid = sessionid;
    }

   
    public int getEntryid() {
        return entryid;
    }

    
    public void setEntryid(int entryid) {
        this.entryid = entryid;
    }

   
    public String getActivity() {
        return activity;
    }

    
    public void setActivity(String activity) {
        this.activity = activity;
    }

    
    public String getRequestresource() {
        return requestresource;
    }

    public void setRequestresource(String requestresource) {
        this.requestresource = requestresource;
    }

   
    public String getTime() {
        return time;
    }

    
    public void setTime(String time) {
        this.time = time;
    }

    
    public String getLoggedinuser() {
        return loggedinuser;
    }

    
    public void setLoggedinuser(String loggedinuser) {
        this.loggedinuser = loggedinuser;
    }

   
    public int getBusinessunit() {
        return businessunit;
    }

   
    public void setBusinessunit(int businessunit) {
        this.businessunit = businessunit;
    }

    
    public String getSourceipaddress() {
        return sourceipaddress;
    }

    
    public void setSourceipaddress(String sourceipaddress) {
        this.sourceipaddress = sourceipaddress;
    }

    
    public String getLoggingfilename() {
        return loggingfilename;
    }

    
    public void setLoggingfilename(String loggingfilename) {
        this.loggingfilename = loggingfilename;
    }

    
    public int getThreadid() {
        return threadid;
    }

    
    public void setThreadid(int threadid) {
        this.threadid = threadid;
    }

    public String getMessage() {
        return message;
    }

    
    public void setMessage(String message) {
        this.message = message;
    }

    
    public String getErrortype() {
        return errortype;
    }

    
    public void setErrortype(String errortype) {
        this.errortype = errortype;
    }

    
    public String getCause() {
        return cause;
    }

    
    public void setCause(String cause) {
        this.cause = cause;
    }

    
    public String getResult() {
        return result;
    }

    
    public void setResult(String result) {
        this.result = result;
    }

    
    public int getSessionid() {
        return sessionid;
    }

    
    public void setSessionid(int sessionid) {
        this.sessionid = sessionid;
    }

   
    
}
