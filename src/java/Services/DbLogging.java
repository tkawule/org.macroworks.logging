
package Services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DbLogging {
    
    private static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    private static final String DB_URL ="jdbc.mysql//localhost/org.macroworks.logging";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "admin";
    
    
    public static Connection GetMySQLConnection() throws IOException, SQLException{
        
        Connection conn = null;
        
                try{
                       
			Class.forName(DB_DRIVER_CLASS);
			
			conn = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
                         
		    } 
                     catch (ClassNotFoundException | SQLException e) {e.getCause(); 
                
                 System.out.println("Database Connection Failed with Message:  "+e.getMessage()+" and cause "+e.getCause()+"");
                }
                if(conn !=null){ 
                    
                  System.out.println("Database Connection Successfull.");
                }
		return conn;
    }
}
