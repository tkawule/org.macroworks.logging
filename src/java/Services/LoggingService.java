
package Services;

import Model.Logging;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class LoggingService {
    
   public static Logging addLogging(Logging logging) throws IOException, SQLException{
       
       try{
           String sql = "{CALL ADDLOGGING(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
           Connection conn = DbLogging.GetMySQLConnection();
           CallableStatement stmt;
           
           stmt = conn.prepareCall(sql);
           stmt.setString("vactivity", logging.getActivity());
           stmt.setString("vrequestresource", logging.getRequestresource());
           stmt.setString("vtime", logging.getTime());
           stmt.setString("vloggedinuser", logging.getLoggedinuser());
           stmt.setInt("vbusinessunit", logging.getBusinessunit());
           stmt.setString("vsourceipaddress", logging.getSourceipaddress());
           stmt.setString("vloggingfilename", logging.getLoggingfilename());
           stmt.setInt("vthreadid", logging.getThreadid());
           stmt.setString("vmessage", logging.getMessage());
           stmt.setString("verrortype", logging.getErrortype());
           stmt.setString("vcause", logging.getCause());
           stmt.setString("result", logging.getResult());
           stmt.setInt("sessionid", logging.getSessionid());
           
           stmt.execute();
       
       }catch (SQLException e){ System.out.println("Add logging Failed: "+e.getMessage()+""); }
       
      return logging;
   }
   
   public static List<Logging> getLoggingList() throws IOException, SQLException{
       
       String sql = "{CALL GETLOGGINGLIST}";
       CallableStatement stmt;
       Connection conn = DbLogging.GetMySQLConnection();
       ResultSet rs;
       List<Logging> loggings = new ArrayList();
       
       try{
           stmt = conn.prepareCall(sql);
           rs = stmt.getResultSet();
           stmt.execute();
           
         while(rs.next()) {
             Logging logging = new Logging();
             logging.setEntryid(rs.getInt("entryid"));
             logging.setActivity(rs.getString("activity"));
             logging.setTime(rs.getString("time"));
             logging.setLoggedinuser(rs.getString("loggedinuser"));
             logging.setBusinessunit(rs.getInt("businessunit"));
             logging.setSourceipaddress(rs.getString("sourceipaddress"));
             logging.setLoggingfilename(rs.getString("loggingfilename"));
             logging.setThreadid(rs.getInt("threadid"));
             logging.setMessage(rs.getString("message"));
             logging.setErrortype(rs.getString("errortype"));
             logging.setCause(rs.getString("cause"));
             logging.setResult(rs.getString("result"));
             logging.setSessionid(rs.getInt("sessionid"));
           
             loggings.add(logging);
         } 
           
       
       }catch(SQLException e) { System.out.println("add get logging list failed");}
       
       return loggings;
   }
   
   
}
